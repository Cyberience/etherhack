# Go parameters
GOCMD=go
GOBUILD=$(GOCMD) build
GOCLEAN=$(GOCMD) clean
GOTEST=$(GOCMD) test
GOGET=$(GOCMD) get
BINARY_NAME=etherhack

all: deps clean build run compile
build:
	$(GOBUILD) -o $(BINARY_NAME) -v
test:
	$(GOTEST) -v ./...
clean:
	$(GOCLEAN)
	rm -f $(BINARY_NAME)
	rm -f $(BINARY_UNIX)
run:
	$(GOBUILD) -o $(BINARY_NAME) -v ./...
	./$(BINARY_NAME)
deps:
	$(GOGET) github.com/ethereum/go-ethereum
	$(GOGET) github.com/VictoriaMetrics/fastcache
	$(GOGET) github.com/deckarep/golang-set
	$(GOGET) github.com/go-stack/stack
	$(GOGET) github.com/shirou/gopsutil/cpu
	$(GOGET) github.com/steakknife/bloomfilter
	$(GOGET) github.com/btcsuite/btcd/btcec

# Cross compilation
compile:
	echo "Compiling for every OS and Platform"
	GOOS=freebsd GOARCH=386 go build -o main-freebsd-386 main.go
	GOOS=linux GOARCH=386 go build -o main-linux-386 main.go
	GOOS=windows GOARCH=386 go build -o main-windows-386 main.go