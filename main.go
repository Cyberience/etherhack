package main

import (
	"bufio"
	"context"
	"crypto/ecdsa"
	_ "encoding/hex"
	"fmt"
	"log"
	"math"
	"math/big"
	"math/rand"
	_ "math/rand"
	"os"
	"regexp"
	"time"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/common/hexutil"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/ethclient"
)

const letterBytes = "abcdef0123456789"
const (
	letterIdxBits = 6                    // 6 bits to represent a letter index
	letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
	letterIdxMax  = 63 / letterIdxBits   // # of letter indices fitting in 63 bits
)

var accounts map[string]bool

func main() {
	defer func() {
		r := recover()
		if r != nil {
			fmt.Print("Error Main logging:", r)
		}
	}()
	//testone("0x0Cb539400641F8949d458b2d5a9e8A3C8bD488f2")
	loadToMap()
	checkAgainstMap()
}

func checkAgainstMap() {
	newBigInt := big.NewInt(int64(0))
	privateKey, _ := crypto.GenerateKey()
	myLog("Looking for more than:", newBigInt)

	for {
		privateKey, _ = crypto.HexToECDSA(RandStringBytesMaskImprSrc(64))
		privateKeyBytes := crypto.FromECDSA(privateKey)
		hexkey := hexutil.Encode(privateKeyBytes)[2:]
		publicKey := privateKey.Public()
		publicKeyECDSA, ok := publicKey.(*ecdsa.PublicKey)
		if !ok {
			log.Fatal("error casting public key to ECDSA")
		}
		address := crypto.PubkeyToAddress(*publicKeyECDSA).Hex()
		if accounts[address] {
			myLog("Found one Address:", address, " Key:", hexkey)
			//testone(address)
		}
		if address[0:6] == "0x0000" { //Output to show still working
			fmt.Print(".")
		}
	}
}

var src1 = rand.NewSource(time.Now().UnixNano())
var src2 = rand.New(rand.NewSource(time.Now().UnixNano()))

// RandStringBytesMaskImprSrc returns a random hexadecimal string of length n.
func RandStringBytesMaskImprSrc(n int) string {
	defer func() {
		r := recover()
		if r != nil {
			fmt.Print("Error RandomString logging:", r)
		}
	}()
	b := make([]byte, n)
	for i, cache, remain := n-1, src1.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = src1.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(letterBytes) {
			b[i] = letterBytes[idx]
			i--
		}
		cache >>= letterIdxBits
		remain--
	}

	return string(b)
}

func testone(addr string) {
	argCount := len(os.Args[1:])
	if argCount > 0 {
		addr = string(os.Args[1])
	}
	fmt.Printf("Balance of %s\n\n", addr)
	re := regexp.MustCompile("^0x[0-9a-fA-F]{40}$")
	fmt.Printf("Testing one Address is valid: %v\n", re.MatchString(addr))
	// client, err := ethclient.Dial("https://snowy-misty-moon.quiknode.io/59882898-188f-4a40-87d0-ffe583b82629/sP5UhscRHq5SEOZKLRm8Izpvwabriid8MfDOKBe-A5b9cVLZEF7svj37Rsdkw6SOs5otx8NekLXwutkGAKdBUg==/") // Quicknode [working]
	// client, err := ethclient.Dial("https://ancient-wild-water.quiknode.pro/9efa724ad737012f1691ff989cbb370a945ac53b/") // Quicknode [failed]

	// client, err := ethclient.Dial("https://api.archivenode.io/t83gs1e8s4hoko138u61t83gowfpm2bd/") // Archive node  [Works]
	// client, _ := ethclient.Dial("http://192.168.0.37:8545")  // Full node
	// client, _ := ethclient.Dial("http://192.168.0.16:8545") //Light node
	client, _ := ethclient.Dial("http://192.168.0.37:8545") // Full node

	account := common.HexToAddress(addr)
	//balance, _ := client.BalanceAt(context.Background(),account,nil)
	balance, _ := client.BalanceAt(context.Background(), account, nil)

	ethBal := new(big.Float)
	ethBal.SetString(balance.String())
	ethValue := new(big.Float).Quo(ethBal, big.NewFloat(math.Pow10(18)))

	fmt.Printf("Balance for account:%s is %d Wei %f Eth\n", addr, balance, ethValue)
}

func loadToMap() {
	accounts = make(map[string]bool)
	linesInFile("data/master01.csv")
	// linesInFile("data/eth07.csv")
	// linesInFile("data/eth01.csv")
	// linesInFile("data/eth05.csv")
	// linesInFile("data/eth06.csv")
	// linesInFile("data/eth11.csv")
	//linesInFile("data/eth02.csv")
	//linesInFile("data/eth04.csv")
	//linesInFile("data/eth03.csv")
	//linesInFile("data/eth08.csv")
	//linesInFile("data/eth09.csv")
	//linesInFile("data/eth10.csv")
}

func linesInFile(fileName string) []string {
	f, _ := os.Open(fileName)
	// Create new Scanner.
	scanner := bufio.NewScanner(f)
	var result []string
	// Use Scan.
	for scanner.Scan() {
		line := scanner.Text()
		//split := strings.Split(line,",")
		accounts[line] = true
	}
	myLog("Size of ", fileName, "is ", len(accounts))
	return result
}
